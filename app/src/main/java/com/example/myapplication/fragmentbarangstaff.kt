package com.example.myapplication

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import com.example.myapplication.R
import com.example.myapplication.R.*
import kotlinx.android.synthetic.main.activity_barang_staff.*
import kotlinx.android.synthetic.main.activity_barang_staff.view.*

class fragmentbarangstaff: Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {

    lateinit var thisParent: laman_staf
    lateinit var lsAdapter: SimpleCursorAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var dialog: AlertDialog.Builder
    lateinit var v: View
    var idBarang: String = ""
    var namaBarang: String = ""
    var passwordUser: String = ""
    var namaKategori: String = ""
    var Stok: String = ""
    var hargajual: String = ""
    var hargabeli: String = ""
    lateinit var db: SQLiteDatabase

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as laman_staf
        v = inflater.inflate(layout.activity_barang_staff, container, false)
        db = thisParent.getdataobjec()
        dialog = AlertDialog.Builder(thisParent)
        v.insert.setOnClickListener(this)
        v.edite.setOnClickListener(this)
        v.del.setOnClickListener(this)
        v.spinner.onItemSelectedListener = this
        v.lsbarang.setOnItemClickListener(click)

        return v
    }

    override fun onStart() {
        super.onStart()
        showDataKategori()
        showBarang()
        //showDataBarang()
        //  showDataBarang()
    }

    fun showDataKategori() {
        val c: Cursor =
            db.rawQuery(
                "select nama_kategori as _id from kategori order by nama_kategori asc",
                null
            )
        spAdapter = SimpleCursorAdapter(
            thisParent,
            android.R.layout.simple_spinner_item,
            c,
            arrayOf("_id"),
            intArrayOf(android.R.id.text1),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner.adapter = spAdapter
        v.spinner.setSelection(0)
    }


    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinner.setSelection(0, true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c: Cursor = spAdapter.getItem(position) as Cursor
        namaKategori = c.getString(c.getColumnIndex("_id"))
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.insert -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang dimasukkan sudah benar?")
                    .setPositiveButton("Ya", btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.edite -> {
                dialog.setTitle("Konfirmasi").setMessage("Data yang dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btup)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.del -> {
                dialog.setTitle("Konfirmasi").setMessage("Yakin akan menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btndel)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.Cari -> {
                //showDataBarang()
            }
        }
    }

    fun insertDataBarang(
        id_barang: String,
        namaBarang: String,
        id_kategori: Int,
        stok: Int,
        hargajual: Int,
        hargabeli: Int
    ) {
        var sql =
            "insert into barang(id_barang,nama_barang, id_kategori, stok, hargajual, hargabeli) values (?,?,?,?,?,?)"
        db.execSQL(sql, arrayOf(id_barang, namaBarang, id_kategori, stok, hargajual, hargabeli))
    }

    fun update(
        id_barang: String,
        namaBarang: String,
        id_kategori: Int,
        stok: Int,
        hargajual: Int,
        hargabeli: Int
    ) {
        var cv: ContentValues = ContentValues()
        cv.put("nama_barang", namaBarang)
        cv.put("id_kategori", id_kategori)
        cv.put("stok", stok)
        cv.put("hargajual", hargajual)
        cv.put("hargabeli", hargabeli)
        db.update("barang", cv, "id_barang= '$id_barang'", null)
        //showDataBarang()
    }

    fun deletDataBarang(idBarang: String) {
        db.delete(" barang ", "id_barang = '$idBarang'", null)
        //showDataBarang()
    }

    fun  insert(id_barang: String,namaBarang: String, katergori : Int,stok: Int,hargajual: Int,hargabeli: Int ){

        var sql =
            "insert into barang(id_barang,nama_barang, id_kategori, stok, hargajual, hargabeli) values (?,?,?,?,?,?)"
        db.execSQL(sql, arrayOf(id_barang, namaBarang, katergori, stok, hargajual, hargabeli))


    }

    val btup= DialogInterface.OnClickListener { dialog, which ->

        var sql = "select id_kategori from kategori where nama_kategori ='$namaKategori'"
        val c: Cursor = db.rawQuery(sql, null)
        if (c.count > 0) {
            c.moveToFirst()
            update(
                v.edidbarang.text.toString(), v.ednamabarang.text.toString(),c.getInt(c.getColumnIndex("id_kategori")), v.edstok.text.toString().toInt(), v.edhargajual.text.toString().toInt(), v.edhargabeli.text.toString().toInt()

            )
            v.edidbarang.setText("")
            v.ednamabarang.setText("")
            v.edstok.setText("")
            v.edhargajual.setText("")
            v.edhargabeli.setText("")
            showBarang()
        }
    }

    val btndel = DialogInterface.OnClickListener { dialog, which ->
        deletDataBarang(idBarang)
        v.edidbarang.setText("")
        v.ednamabarang.setText("")
        v.edstok.setText("")
        v.edhargajual.setText("")
        v.edhargabeli.setText("")
        showBarang()
    }




    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_kategori from kategori where nama_kategori ='$namaKategori'"
        val c: Cursor = db.rawQuery(sql, null)
        if (c.count > 0) {
            c.moveToFirst()
            insert(
                v.edidbarang.text.toString(),
                v.ednamabarang.text.toString(),
                c.getInt(c.getColumnIndex("id_kategori")),
                v.edstok.text.toString().toInt(),
                v.edhargajual.text.toString().toInt(),
                v.edhargabeli.text.toString().toInt()

            )
            v.edidbarang.setText("")
            v.ednamabarang.setText("")
            v.edstok.setText("")
            v.edhargajual.setText("")
            v.edhargabeli.setText("")
            showBarang()
        }
    }



    val click = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        idBarang = c.getString(c.getColumnIndex("_id"))
        v.edidbarang.setText(c.getString(c.getColumnIndex("_id")))
        v.ednamabarang.setText(c.getString(c.getColumnIndex("nama_barang")))
        v.edstok.setText(c.getString(c.getColumnIndex("stok")))
        v.edhargajual.setText(c.getString(c.getColumnIndex("hargajual")))
        v.edhargabeli.setText(c.getString(c.getColumnIndex("hargabeli")))
        //v.inputnil.setText(c.getString(c.getColumnIndex("nama_jenis")))

    }

    fun showBarang() {
        var sql = " select id_barang as _id , nama_barang , nama_kategori, stok , hargajual , hargabeli from barang , kategori where barang.id_kategori=kategori.id_kategori"
        val c: Cursor = db.rawQuery(sql, null)
        lsAdapter = SimpleCursorAdapter(
            thisParent,
            layout.isi_barang,
            c,
            arrayOf("_id", "nama_barang","nama_kategori","stok","hargajual","hargabeli"),
            intArrayOf(R.id.txid_user, R.id.txnamabarang,R.id.txkategori,R.id.txstok,R.id.txhargajual,R.id.txhargabeli),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        v.lsbarang.adapter = lsAdapter
    }


}