package com.example.myapplication

import android.app.AlertDialog
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_transaksi_staff.*
import kotlinx.android.synthetic.main.activity_transaksi_staff.view.*

class fragmenttrxstaff : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btpil->{
                beli(id_bar.text.toString())
            }
            R.id.insert2->{
                tampil()

            }
        }

    }



    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
    }

    lateinit var thisParent: laman_staf
    lateinit var lsAdapter: ArrayAdapter<String>
    lateinit var dialog: AlertDialog.Builder
    lateinit var v: View
    lateinit var  arrayList: ArrayList<String>
    lateinit var db: SQLiteDatabase




    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as laman_staf
        v = inflater.inflate(R.layout.activity_transaksi_staff, container, false)
        db = thisParent.getdataobjec()
        dialog = AlertDialog.Builder(thisParent)
        v.insert2.setOnClickListener(this)
        v.del2.setOnClickListener(this)
        v.btpil.setOnClickListener(this)

        return v
    }



    fun beli(namab : String){
        val sql = "select a.nama_barang,b.nama_kategori,a.hargajual from Barang a, kategori b where a.id_kategori=b.id_kategori  AND id_barang='$namab'"
        val c : Cursor = db.rawQuery(sql,null)
        c.moveToFirst()
        namabarang.setText(c.getString(0))
        kategori.setText(c.getString(1))
        harga.setText(c.getString(2))

    }

    fun tampil(){
        var a = namabarang.text.toString()
        var b = kategori.text.toString()
        var c =  stok.text.toString()
        var jumlah =stok.text.toString()
        var hargaa = harga.text.toString()
        var total = jumlah.toInt()*hargaa.toInt()
        var has = "Nama Barang:"+a+"Kategori:"+b+"Item:"+c+"Total :"+total
        arrayList = ArrayList()
        arrayList.add(has)

        var adap = ArrayAdapter(activity!!,android.R.layout.simple_list_item_1,arrayList)
        v.lsTransaksi.adapter  = adap



    }
}