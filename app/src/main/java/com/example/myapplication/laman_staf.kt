package com.example.myapplication

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.example.myapplication.R
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_staff.*


class laman_staf : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener  {
    //override fun onNavigationItemSelected(item: MenuItem): Boolean {
     //   TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    //}

    lateinit var fragmentbarangstaff: fragmentbarangstaff
    lateinit var  fragmenttrxstaff: fragmenttrxstaff
    lateinit var fragmentProfile: fragment_profile
    lateinit var ft : FragmentTransaction
    lateinit var db : SQLiteDatabase
    //lateinit var thisparent : laman_staf
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_staff)
        botom.setOnNavigationItemSelectedListener(this)
        fragmentbarangstaff =  fragmentbarangstaff()
        fragmenttrxstaff = fragmenttrxstaff()
        fragmentProfile = fragment_profile()
        db = OpenHelper(this).writableDatabase
    }

    fun  getdataobjec(): SQLiteDatabase {
        return db
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.itmbarang->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.aa,fragmentbarangstaff).commit()
                aa.setBackgroundColor(Color.argb(255,245,255,245))
                aa.visibility= View.VISIBLE
            }
            R.id.itmtransaksi->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.aa,fragmenttrxstaff).commit()
                aa.setBackgroundColor(Color.argb(255,245,255,245))
                aa.visibility= View.VISIBLE
            }
            R.id.itmeditprofil->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.aa,fragmentProfile).commit()
                aa.setBackgroundColor(Color.argb(255,245,255,245))
                aa.visibility= View.VISIBLE
            }
            R.id.itemabout-> aa.visibility =  View.GONE
        }
        return true

    }
}

